package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/application"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
)

func init() {
	application.Init("eventexporter.yml")
}

func main() {
	// Create, initialize and start application objects
	app := application.NewAppObjects()

	// Set interrupt handler
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	app.Start()
	logger.Info("Application started. Press Ctrl+C to exit...")

OUTER:
	for {
		select {
		// Wait for user or OS interrupt
		case <-done:
			break OUTER

		// Catch broker connection notification
		case connErr := <-app.Manager.Done:
			if connErr != nil {
				// Recreate application objects
				app.Stop()
				time.Sleep(time.Second)
				app.Start()
			}
		}
	}

	app.Stop()
	logger.Info("Application exited properly")
}
