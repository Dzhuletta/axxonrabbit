// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build windows

package main

import (
	"fmt"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/application"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"golang.org/x/sys/windows/svc"
)

func init() {
	application.Init("eventexporterservice.yml")
}

type myservice struct{}

func (m *myservice) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	const cmdsAccepted = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue
	changes <- svc.Status{State: svc.StartPending}

	// Create, initialize and start application objects
	app := application.NewAppObjects()
	app.Start()
	logger.Info("Application started")

	changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}

loop:
	for {
		select {
		// Catch broker connection notification
		case connErr := <-app.Manager.Done:
			if connErr != nil {
				// Recreate application objects
				app.Stop()
				time.Sleep(time.Second)
				app.Start()
			}
		case c := <-r:
			switch c.Cmd {
			case svc.Interrogate:
				changes <- c.CurrentStatus
				// Testing deadlock from https://code.google.com/p/winsvc/issues/detail?id=4
				time.Sleep(100 * time.Millisecond)
				changes <- c.CurrentStatus
			case svc.Stop, svc.Shutdown:
				app.Stop()
				logger.Info("Application exited properly")
				break loop
			default:
				logger.Error(fmt.Sprintf("unexpected control request #%d", c))
			}
		}
	}
	changes <- svc.Status{State: svc.StopPending}
	return
}

func runService(name string, isDebug bool) {
	logger.Info(fmt.Sprintf("starting %s service", name))
	run := svc.Run
	err := run(name, &myservice{})
	if err != nil {
		logger.Error(fmt.Sprintf("%s service failed: %v", name, err))
		return
	}
	logger.Info(fmt.Sprintf("%s service stopped", name))
}
