package entities

import "bitbucket.org/Axxonsoft/bl/events"

type Event events.DetectorEvent

func (e Event) Type() string {
	return "AxxonNext event"
}

func (e Event) GetTimestamp() string {
	return e.Timestamp
}

type EventChannel chan Event

// NewEventChannel produces ordinary or blocking channel for events
func NewEventChannel(size int64) EventChannel {
	if size < 0 {
		return make(EventChannel)
	}
	return make(EventChannel, size)
}
