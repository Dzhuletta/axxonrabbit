package interfaces

// Event
type Event interface {
	Type() string
	GetTimestamp() string
}
