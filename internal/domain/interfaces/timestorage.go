package interfaces

type TimeStorage interface {
	Get() string
	Save(timestamp string)
	Start()
	Stop()
}
