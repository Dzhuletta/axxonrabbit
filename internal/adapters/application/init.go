package application

import (
	"flag"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func Init(defaultConfigFilename string) {
	// Get project directory
	path, err := filepath.Abs(os.Args[0])
	if err != nil {
		log.Fatalf("failed getting project directory: %v", err)
	}
	projectDir := filepath.Dir(path) + "/.."

	// using standard library "flag" package
	defaultConfigPath := projectDir + "/configs/" + defaultConfigFilename
	flag.String("config", defaultConfigPath, "path to configuration flag")
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	_ = viper.BindPFlags(pflag.CommandLine)

	// Check config file existence and read configuration from it
	configPath := viper.GetString("config") // retrieve value from viper
	if _, err := os.Stat(configPath); err == nil {
		viper.SetConfigFile(configPath)
		if err := viper.ReadInConfig(); err != nil {
			log.Fatalf("Couldn't read configuration file: %s", err.Error())
		}
	}

	// Setting log parameters
	logger.Init(viper.GetString("log.level"), viper.GetString("log.file"))
}
