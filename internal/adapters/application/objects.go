package application

import (
	"context"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/broker"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/nativebl"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/storage"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/entities"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"
	"github.com/spf13/viper"
)

// AppObjects data and objects for application
type AppObjects struct {
	ctx          context.Context
	cancel       context.CancelFunc
	eventChannel entities.EventChannel
	eventFetcher *nativebl.EventFetcher
	eventSender  *broker.EventSender
	timeStorage  interfaces.TimeStorage
	Manager      *broker.Manager
}

// NewAppObjects constructs default application objects.
func NewAppObjects() *AppObjects {
	return &AppObjects{}
}

func (app *AppObjects) Start() {
	// Make cancel context
	app.ctx, app.cancel = context.WithCancel(context.Background())

	// Create time storage
	app.timeStorage = storage.NewFileTimeStorage(
		viper.GetString("storage.file"),
		viper.GetUint64("storage.intervalMs"))
	if app.timeStorage == nil {
		logger.Fatal("failed creating time storage")
	}
	logger.Info("File time storage created")

	// Create event channel
	app.eventChannel = entities.NewEventChannel(viper.GetInt64("events.limit"))
	if app.eventChannel == nil {
		logger.Fatal("failed creating event channel")
	}
	logger.Info("Event channel created")

	// Create broker Manager
	app.Manager = broker.NewManager(viper.GetString("amqp.url"))
	if app.Manager == nil {
		logger.Fatal("failed connecting to RabbitMQ")
	}
	logger.Info("RabbitMQ broker created", "host", viper.GetString("amqp.host"))

	// Reconnect broker connection
	if err := app.Manager.Reconnect(app.ctx); err != nil {
		logger.Error("error reconnecting RabbitMQ", "error", err)
	}

	// Start broker connection listener
	go app.Manager.ConnectionListener(app.ctx)

	// Start file time storage
	go app.timeStorage.Start()

	// Create event sender object
	app.eventSender = broker.NewEventSender(
		app.ctx, app.eventChannel,
		app.Manager.Conn, app.timeStorage,
		viper.GetString("amqp.exchange"),
		viper.GetString("amqp.queue"),
		viper.GetString("amqp.routing"))
	if app.eventSender == nil {
		logger.Fatal("failed creating event sender")
	}
	logger.Info("Event sender created")

	// Start event sender
	go app.eventSender.Start()

	// Create and start AxxonNext event fetcher object
	app.eventFetcher = nativebl.NewEventFetcher(
		app.ctx, app.eventChannel, app.timeStorage,
		viper.GetString("nativebl.address"),
		viper.GetString("nativebl.user"),
		viper.GetString("nativebl.password"),
		viper.GetUint64("events.limit"),
		viper.GetUint64("events.intervalSec"),
	)
	if app.eventFetcher == nil {
		logger.Fatal("failed creating AxxonNext event fetcher")
	}
	logger.Info("Event fetcher created")

	// Start AxxonNext event fetcher
	go app.eventFetcher.Start()
}

func (app *AppObjects) Stop() {
	app.cancel()
	app.eventFetcher.Stop()
	app.eventSender.Stop()
	app.timeStorage.Stop()
	if err := app.Manager.Close(); err != nil {
		logger.Error("failed stopping RabbitMQ connection", "error", err)
	}
	close(app.eventChannel)
	logger.Info("Stopped application objects")
}
