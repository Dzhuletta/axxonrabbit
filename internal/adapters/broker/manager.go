package broker

import (
	"context"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"github.com/cenkalti/backoff/v4"
)

type Manager struct {
	connURL string
	Conn    *amqp.Connection
	Done    chan error
}

func NewManager(connURL string) *Manager {
	// Create manager object
	m := &Manager{
		connURL: connURL,
		Conn:    nil,
		Done:    make(chan error),
	}

	return m
}

func (m *Manager) connect() (err error) {
	// Open RabbitMQ connection
	m.Conn, err = amqp.Dial(m.connURL)
	if err != nil {
		return err
	}

	return nil
}

func (m *Manager) ConnectionListener(ctx context.Context) {
	select {
	case <-ctx.Done():
		break
	case connErr := <-m.Conn.NotifyClose(make(chan *amqp.Error)):
		if connErr != nil {
			logger.Error("RabbitMQ connection is closed", "error", connErr.Error())
		}
		// Notify clients
		m.Done <- errors.New("RabbitMQ connection is closed")
	}
}

func (m *Manager) Reconnect(ctx context.Context) error {
	// Create reconnect backoff
	be := backoff.NewExponentialBackOff()
	be.MaxElapsedTime = 0 // Never stops
	be.InitialInterval = 1 * time.Second
	be.Multiplier = 2
	be.MaxInterval = 15 * time.Second

	// Do reconnect loop
	boCtx := backoff.WithContext(be, context.Background())
	for {
		boTime := boCtx.NextBackOff()
		if boTime == backoff.Stop {
			return errors.New("backoff timer elapsed")
		}

		select {
		case <-ctx.Done():
			return nil
		case <-time.After(boTime):
			if err := m.connect(); err != nil {
				logger.Error("couldn't reconnect", "error", err)
				continue
			}
			logger.Info("Reconnect to RabbitMQ succeeded")
			return nil
		}
	}
}

func (m *Manager) Close() error {
	// Close connection
	if m.Conn != nil {
		if err := m.Conn.Close(); err != nil {
			logger.Error("failed closing connection", "error", err)
		}
	}

	return nil
}
