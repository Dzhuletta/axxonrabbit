package broker

import (
	"encoding/json"
	"fmt"
	"strings"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"github.com/pkg/errors"

	"github.com/streadway/amqp"
)

type AMQPWriter struct {
	cwq          *ChannelWithQueue
	exchangeName string
	routingKey   string
}

func NewAMQPWriter(conn *amqp.Connection, timeStorage interfaces.TimeStorage, exchangeName, queueName, routingKey string) *AMQPWriter {
	// Create amqp channel and queue
	ch, err := NewChannelWithQueue(conn, exchangeName, queueName, routingKey)
	if err != nil {
		logger.Error("failed creating amqp channel and queue",
			"error", err,
			"caller", "NewAmqpWriter")
		return nil
	}

	return &AMQPWriter{
		cwq:          ch,
		exchangeName: exchangeName,
		routingKey:   routingKey,
	}
}

// WriteEnvelope sends AMQP envelope to RabbitMQ broker, returns error object or nil
func (w *AMQPWriter) WriteEnvelope(env *AMQPEnvelope) error {
	if w.cwq.Ch == nil {
		return errors.New("no output channel defined")
	}

	// Marshall message to JSON
	buffer, err := json.Marshal(env.Event)
	if err != nil {
		return errors.Wrap(err, "error marshalling RPC request to JSON")
	}

	// Remove JSON umbrella tag for protobuf oneof case
	// See https://support.axxonsoft.com/jira/browse/CLOUD-5930
	// TODO: Make JSON marshalling with protojson package
	correctedBuffer, err := correctTags(buffer)
	if err != nil {
		return errors.Wrap(err, "error correcting JSON tags")
	}

	// Send message with metadata to gateway queue
	err = w.cwq.Ch.Publish(
		w.exchangeName, // exchange
		w.routingKey,   // routing key
		false,          // mandatory
		false,          // immediate
		amqp.Publishing{
			DeliveryMode:  amqp.Persistent,
			ContentType:   "application/json",
			CorrelationId: env.Metadata.CorrelationID,
			Type:          env.Metadata.Type,
			Body:          correctedBuffer,
		})
	if err != nil {
		return errors.Wrap(err, "failed to publish a message")
	}

	return nil
}

func (w *AMQPWriter) Close() error {
	if err := w.cwq.Close(); err != nil {
		return errors.Wrap(err, "failed closing writer channel")
	}
	return nil
}

func correctTags(message []byte) ([]byte, error) {
	input := string(message)

	// Fix capitalized tags
	input = strings.ReplaceAll(input, "Rectangle", "rectangle")
	input = strings.ReplaceAll(input, "AutoRecognitionResult", "autoRecognitionResult")
	input = strings.ReplaceAll(input, "FaceRecognitionResult", "faceRecognitionResult")
	input = strings.ReplaceAll(input, "ListedItemDetectedResult", "listedItemDetectedResult")
	input = strings.ReplaceAll(input, "QueueDetectedResult", "queueDetectedResult")
	input = strings.ReplaceAll(input, "MoveInZone", "moveInZone")
	input = strings.ReplaceAll(input, "ByteVector", "byteVector")
	input = strings.ReplaceAll(input, "GeoPosition", "geoPosition")

	// Fix camel capped tags
	input = strings.ReplaceAll(input, "origin_deprecated", "originDeprecated")
	input = strings.ReplaceAll(input, "event_type", "eventType")
	input = strings.ReplaceAll(input, "origin_ext", "originExt")
	input = strings.ReplaceAll(input, "access_point", "accessPoint")
	input = strings.ReplaceAll(input, "friendly_name", "friendlyName")
	input = strings.ReplaceAll(input, "detector_ext", "detectorExt")
	input = strings.ReplaceAll(input, "node_info", "nodeInfo")

	var decoded interface{}
	err := json.Unmarshal([]byte(input), &decoded)
	if err != nil {
		return nil, fmt.Errorf("error mashalling string: %s", err.Error()) //nolint:goerr113
	}
	decodedMap, ok := decoded.(map[string]interface{})
	if !ok {
		return nil, errors.New("error decoding map")
	}
	delete(decodedMap, "detector_deprecated")

	for key, value := range decodedMap {
		if key == "details" {
			values, ok := value.([]interface{})
			if !ok {
				return nil, errors.New("error decoding interfaces")
			}
			newValues := make([]interface{}, 0, len(values)) //map[string]interface{})
			for _, item := range values {
				itemValue, ok := item.(map[string]interface{})
				if !ok {
					return nil, errors.New("error decoding item value")
				}
				itemValueMap, ok := itemValue["Value"].(map[string]interface{})
				if !ok {
					return nil, errors.New("error decoding item value map")
				}
				newValues = append(newValues, itemValueMap)
			}
			decodedMap["details"] = newValues
		}
	}

	result, err := json.Marshal(decodedMap)
	if err != nil {
		return nil, errors.Wrap(err, "error marshalling result")
	}

	return result, nil
}
