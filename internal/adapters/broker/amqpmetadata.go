package broker

import (
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"
	"github.com/google/uuid"
)

// AMQPMetadata holds extra data for AMQP message
type AMQPMetadata struct {
	CorrelationID string
	Type          string
}

// NewCorrelationID returns correlation UUID
func NewCorrelationID() string {
	return uuid.New().String()
}

// AMQPEnvelope holds message with AMQP metadata
type AMQPEnvelope struct {
	Event    interfaces.Event
	Metadata *AMQPMetadata
}

// NewEnvelope creates and fills message envelope
func NewEnvelope(event interfaces.Event) *AMQPEnvelope {
	return &AMQPEnvelope{
		Event: event,
		Metadata: &AMQPMetadata{
			CorrelationID: NewCorrelationID(),
			Type:          event.Type(),
		},
	}
}
