package broker //nolint:testpackage

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

var messageTests = []struct {
	actual   string
	expected string
}{
	{
		actual: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "origin_deprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "origin_ext" : {
						"access_point" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendly_name" : "Дверь_1"
					  },
					  "detector_deprecated" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier",
					  "detector_ext" : {
						"access_point" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "node_info" : {
						"name" : "NDTPID180300164"
					  },
					  "event_type" : "faceAppeared",
					  "details" : [ {
						"Value" : {
						  "Rectangle" : {
							"x" : 0.3363888888888889,
							"y" : 0.42812500000000003,
							"w" : 0.06611111111111112,
							"h" : 0.12638888888888894,
							"index" : 136
						  }
						}
					  }, {
						"Value" : {
						  "FaceRecognitionResult" : {
							"temperature" : {
							  "value" : -1000
							}
						  }
						}
					  } ]
					}`,
		expected: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "originDeprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "originExt" : {
						"accessPoint" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendlyName" : "Дверь_1"
					  },
					  "detectorExt" : {
						"accessPoint" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "nodeInfo" : {
						"name" : "NDTPID180300164"
					  },
					  "eventType" : "faceAppeared",
					  "details" : [ {
						  "rectangle" : {
							"x" : 0.3363888888888889,
							"y" : 0.42812500000000003,
							"w" : 0.06611111111111112,
							"h" : 0.12638888888888894,
							"index" : 136
						  }
					  }, {
						  "faceRecognitionResult" : {
							"temperature" : {
							  "value" : -1000
							}
						  }
					  } ]
					}`,
	},
	{
		actual: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "origin_deprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "origin_ext" : {
						"access_point" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendly_name" : "Дверь_1"
					  },
					  "detector_deprecated" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier",
					  "detector_ext" : {
						"access_point" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "node_info" : {
						"name" : "NDTPID180300164"
					  },
					  "event_type" : "faceAppeared",
					  "details" : [ {
						"Value" : {
						  "Rectangle" : {
							"x" : 0.3363888888888889,
							"y" : 0.42812500000000003,
							"w" : 0.06611111111111112,
							"h" : 0.12638888888888894,
							"index" : 136
						  }
						}
					  } ]
					}`,
		expected: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "originDeprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "originExt" : {
						"accessPoint" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendlyName" : "Дверь_1"
					  },
					  "detectorExt" : {
						"accessPoint" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "nodeInfo" : {
						"name" : "NDTPID180300164"
					  },
					  "eventType" : "faceAppeared",
					  "details" : [ {
						  "rectangle" : {
							"x" : 0.3363888888888889,
							"y" : 0.42812500000000003,
							"w" : 0.06611111111111112,
							"h" : 0.12638888888888894,
							"index" : 136
						  }
					  } ]
					}`,
	},
	{
		actual: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "origin_deprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "origin_ext" : {
						"access_point" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendly_name" : "Дверь_1"
					  },
					  "detector_deprecated" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier",
					  "detector_ext" : {
						"access_point" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "node_info" : {
						"name" : "NDTPID180300164"
					  },
					  "event_type" : "faceAppeared",
					  "details" : [ ]
					}`,
		expected: `{
					  "guid" : "f88d17a6-7ee7-404c-a253-41307fb565f7",
					  "timestamp" : "20210311T031131.895000",
					  "originDeprecated" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
					  "originExt" : {
						"accessPoint" : "hosts/NDTPID180300164/DeviceIpint.92099_3/SourceEndpoint.video:0:0",
						"friendlyName" : "Дверь_1"
					  },
					  "detectorExt" : {
						"accessPoint" : "hosts/NDTPID180300164/AVDetector.3/EventSupplier"
					  },
					  "nodeInfo" : {
						"name" : "NDTPID180300164"
					  },
					  "eventType" : "faceAppeared",
					  "details" : [ ]
					}`,
	},
}

func TestCorrectTags(t *testing.T) {
	for i, tt := range messageTests {
		t.Run(fmt.Sprintf("Test %d", i), func(t *testing.T) {
			result, err := correctTags([]byte(tt.actual)) //nolint:scopelint
			require.NoError(t, err)
			areEqual, err := areEqualJSON(tt.expected, string(result)) //nolint:scopelint
			require.NoError(t, err)
			require.True(t, areEqual)
		})
	}
}

func areEqualJSON(s1, s2 string) (bool, error) {
	var o1 interface{}
	var o2 interface{}

	var err error
	err = json.Unmarshal([]byte(s1), &o1)
	if err != nil {
		return false, fmt.Errorf("error mashalling string 1 :: %s", err.Error()) //nolint:goerr113
	}
	err = json.Unmarshal([]byte(s2), &o2)
	if err != nil {
		return false, fmt.Errorf("error mashalling string 2 :: %s", err.Error()) //nolint:goerr113
	}

	return reflect.DeepEqual(o1, o2), nil
}
