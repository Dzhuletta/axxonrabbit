package broker

import (
	"context"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/entities"

	"github.com/streadway/amqp"
)

type EventSender struct {
	ctx         context.Context
	events      entities.EventChannel
	timeStorage interfaces.TimeStorage
	client      *AMQPWriter
}

func NewEventSender(
	ctx context.Context, events entities.EventChannel,
	conn *amqp.Connection, timeStorage interfaces.TimeStorage,
	exchange, queue, routing string,
) *EventSender {
	client := NewAMQPWriter(conn, timeStorage, exchange, queue, routing)
	if client == nil {
		return nil
	}
	return &EventSender{
		ctx:         ctx,
		events:      events,
		timeStorage: timeStorage,
		client:      client,
	}
}

func (es *EventSender) Start() {
	logger.Info("Starting event sender")
	for {
		select {
		case <-es.ctx.Done():
			return
		case event := <-es.events:
			if err := es.client.WriteEnvelope(NewEnvelope(event)); err != nil {
				logger.Error("failed sending message", "error", err)
			}
			// Save event timestamp
			es.timeStorage.Save(event.GetTimestamp())
		}
	}
}

func (es *EventSender) Stop() {
	_ = es.client.Close()
	logger.Info("Event sender stopped")
}
