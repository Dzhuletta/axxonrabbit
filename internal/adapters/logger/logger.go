package logger

import (
	"fmt"
	"log"
	"net/url"
	"sync"

	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"

	"go.uber.org/zap"
)

var (
	sugar *zap.SugaredLogger
	once  sync.Once
)

type lumberjackSink struct {
	*lumberjack.Logger
}

func (lumberjackSink) Sync() error {
	return nil
}

// Init initializes a thread-safe singleton logger
// This would be called from a main method when the application starts up
// This function would ideally, take zap configuration, but is left out
// in favor of simplicity using the example logger.
func Init(logLevel, filePath string) {
	// once ensures the singleton is initialized only once
	once.Do(func() {
		config := zap.NewProductionConfig()
		config.OutputPaths = []string{
			fmt.Sprintf("lumberjack:%s", filePath),
		}
		config.ErrorOutputPaths = []string{
			fmt.Sprintf("lumberjack:%s", filePath),
		}
		config.EncoderConfig = zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.LowercaseLevelEncoder,

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,

			EncodeDuration: zapcore.NanosDurationEncoder,
		}

		var level zapcore.Level
		err := level.UnmarshalText([]byte(logLevel))
		if err != nil {
			log.Fatalf("can't marshal level string: %v", logLevel)
		}
		config.Level = zap.NewAtomicLevelAt(level)

		ll := lumberjack.Logger{
			Filename:   filePath,
			MaxSize:    10, //MB
			MaxBackups: 100,
			MaxAge:     30, //days
			Compress:   true,
		}
		err = zap.RegisterSink("lumberjack", func(*url.URL) (zap.Sink, error) {
			return lumberjackSink{
				Logger: &ll,
			}, nil
		})
		if err != nil {
			log.Fatalf("can't initialize limberjack sink: %v", err)
		}

		logger, err := config.Build()
		if err != nil {
			log.Fatalf("can't initialize zap logger: %v", err)
		}
		defer func() {
			if err := logger.Sync(); err != nil {
				return
				// Do not process sync err as told in:
				// https://github.com/uber-go/zap/issues/328
				//log.Fatalf("can't sync logger: %v", err)
			}
		}()
		sugar = logger.Sugar()
	})
}

// Debug logs a debug message with the given fields
func Debug(message string, fields ...interface{}) {
	sugar.Debugw(message, fields...)
}

// Info logs a debug message with the given fields
func Info(message string, fields ...interface{}) {
	sugar.Infow(message, fields...)
}

// Warn logs a debug message with the given fields
func Warn(message string, fields ...interface{}) {
	sugar.Warnw(message, fields...)
}

// Error logs a debug message with the given fields
func Error(message string, fields ...interface{}) {
	sugar.Errorw(message, fields...)
}

// Fatal logs a message than calls os.Exit(1)
func Fatal(message string, fields ...interface{}) {
	sugar.Fatalw(message, fields...)
}
