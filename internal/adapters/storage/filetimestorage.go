package storage

import (
	"io/ioutil"
	"os"
	"sync"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"
)

type FileTimeStorage struct {
	filename         string
	mx               sync.Mutex
	timestamp        string
	timestampWritten string
	ticker           *time.Ticker
	done             chan struct{}
}

func NewFileTimeStorage(filename string, intervalMs uint64) interfaces.TimeStorage {
	return &FileTimeStorage{
		filename: filename,
		ticker:   time.NewTicker(time.Duration(intervalMs) * time.Millisecond),
		done:     make(chan struct{}),
	}
}

func (s *FileTimeStorage) Get() string {
	s.mx.Lock()
	timestamp := s.timestamp
	s.mx.Unlock()

	if len(timestamp) == 0 {
		// Check stored timestamp
		if _, err := os.Stat(s.filename); err == nil {
			stored, err := ioutil.ReadFile(s.filename)
			if err != nil {
				logger.Error("failed reading timestamp", "error", err, "file", s.filename)
				return ""
			}
			timestamp = string(stored)
		}
	}

	return timestamp
}

func (s *FileTimeStorage) Save(timestamp string) {
	s.mx.Lock()
	s.timestamp = timestamp
	s.mx.Unlock()
}

func (s *FileTimeStorage) Start() {
	logger.Info("Starting file time storage")
	for {
		select {
		case <-s.done:
			return
		case <-s.ticker.C:
			s.write()
		}
	}
}

func (s *FileTimeStorage) write() {
	s.mx.Lock()
	timestamp := s.timestamp
	s.mx.Unlock()

	if timestamp == s.timestampWritten {
		return
	}

	f, err := os.OpenFile(s.filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		logger.Error("failed opening file to write", "error", err, "file", s.filename)
	}

	written, err := f.WriteString(timestamp)
	if err != nil {
		logger.Error("failed writing timestamp",
			"error", err, "file", s.filename, "timestamp", timestamp)
	}
	if written != len(timestamp) {
		logger.Error("failed writing full content",
			"file", s.filename, "timestamp", timestamp)
	}

	if err = f.Sync(); err != nil {
		logger.Error("error syncing file", "error", err, "file", s.filename)
	}

	if err := f.Close(); err != nil {
		logger.Error("error closing file", "error", err, "file", s.filename)
	}

	s.timestampWritten = timestamp
}

func (s *FileTimeStorage) Stop() {
	s.ticker.Stop()
	s.done <- struct{}{}
	s.write()
	logger.Info("File time storage stopped")
}
