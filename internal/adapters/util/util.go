package util

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"

	"github.com/go-errors/errors"
	"golang.org/x/net/context"
)

// CheckErr check error is nil and if not panic with message
func CheckErr(err error, msg ...string) {
	if err != nil {
		log.Panicln(msg, err)
	}
}

// CheckOk check that ok and if not panic with message
func CheckOk(ok bool, msg string) {
	if !ok {
		log.Panicln(msg)
	}
}

func ParseTime(timeStr string) (time.Time, error) {
	return time.Parse("20060102T150405.000000", timeStr)
}

var emptyFun = func() {}

func WaitUntil(ctx context.Context, times int, repeatInterval, ctxTimeout time.Duration, actionName string, action func(ctx context.Context) (stop bool, err error)) (err error) {
	logger.Debug("repeating action started", "actionName", actionName,
		"repeatInterval", repeatInterval, "times", times)

	var stop bool
	for i := 0; ; {
		logger.Debug("Iteration started", "actionName", actionName, "i", i)

		iterationCtx := ctx
		cancel := emptyFun

		if ctxTimeout != 0 {
			iterationCtx, cancel = context.WithTimeout(ctx, ctxTimeout)
		}

		func() {
			defer cancel()
			stop, err = action(iterationCtx)
		}()

		if err != nil {
			logger.Error("failed iteration", "actionName", actionName, "i", i,
				"error", err, "stop", stop)
		} else {
			logger.Debug("Iteration finished", "actionName", actionName, "i", i, "stop", stop)
		}

		if stop {
			logger.Debug("Action stopped", "actionName", actionName, "i", i)
			return err
		}

		i++
		if i >= times {
			i = 0
		}

		select {
		case <-time.NewTimer(repeatInterval).C:
			continue
		case <-ctx.Done():
			logger.Debug("Context canceled", "actionName", actionName, "i", i)
			return context.Canceled
		}
	}
	return errors.WrapPrefix(err, fmt.Sprintf("%s failed %d times", actionName, times), 0) // nolint: govet
}
