package nativebl

import (
	"context"
	"math"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/util"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/interfaces"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/entities"
)

// EventFetcher data
type EventFetcher struct {
	ctx         context.Context
	client      *Client
	timeStorage interfaces.TimeStorage
	limit       uint64
	ticker      *time.Ticker
}

// NewEventFetcher constructs object for event fetching from AxxonNext.
func NewEventFetcher(
	ctx context.Context, events entities.EventChannel, timeStorage interfaces.TimeStorage,
	address, user, password string,
	limit uint64,
	intervalSec uint64,
) *EventFetcher {
	client := NewClient(address, user, password, events)
	if client == nil {
		logger.Error("failed creating NativeBL client")
		return nil
	}

	return &EventFetcher{
		ctx:         ctx,
		client:      client,
		timeStorage: timeStorage,
		limit:       limit,
		ticker:      time.NewTicker(time.Duration(intervalSec) * time.Second),
	}
}

func (ef *EventFetcher) Start() {
	logger.Info("Starting event fetcher")
	for {
		select {
		case <-ef.ctx.Done():
			logger.Debug("Exited from reading events")
			return
		case t := <-ef.ticker.C:
			var beginTime time.Time
			timestamp := ef.timeStorage.Get()
			if len(timestamp) != 0 {
				parsed, err := util.ParseTime(timestamp)
				if err != nil {
					logger.Error("failed parsing timestamp", "error", err, "time", timestamp)
					continue
				}
				beginTime = parsed
			}
			beginTime = beginTime.Add(time.Millisecond) // shift time from last event

			// First query to evaluate end time
			events, err := ef.client.ReadEvents(beginTime, t.UTC(), ef.limit)
			if err != nil {
				logger.Error("failed reading events", "error", err)
			}
			if len(events) == 0 {
				continue
			}
			parsed, err := util.ParseTime(events[len(events)-1].Timestamp)
			if err != nil {
				logger.Error("failed parsing timestamp", "error", err, "time", timestamp)
				continue
			}
			endTime := parsed

			// Second query to get all events within time interval
			events, err = ef.client.ReadEvents(beginTime, endTime, math.MaxUint64)
			if err != nil {
				logger.Error("failed reading events", "error", err)
			}

			// Sent events to other goroutine
			ef.client.SendEvents(events)
		}
	}
}

func (ef *EventFetcher) Stop() {
	ef.ticker.Stop()
	ef.client.Close()
	logger.Info("Event fetcher stopped")
}
