package model

type NodeAddr struct {
	Name             string
	Address          string
	BLAddr           string
	EndpointsFullURL string
	WebServerAddr    string
	CorbaPort        int
}

// GetBlAddress returns URI of BL
func (n *NodeAddr) GetBlAddress() string {
	return n.BLAddr
}

// GetName returns name of node
func (n *NodeAddr) GetName() string {
	return n.Name
}
