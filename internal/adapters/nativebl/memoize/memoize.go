package memoize

import (
	"time"

	"github.com/karlseguin/ccache"
	"golang.org/x/sync/singleflight"
)

// Memoizer allows you to memoize function calls. Memoizer is safe for concurrent use by multiple goroutines.
type Memoizer struct {

	// Storage exposes the underlying cache of memoized results to manipulate as desired - for example, to Flush().
	Storage *ccache.Cache

	group singleflight.Group

	valueExpiration time.Duration
	errExpiration   time.Duration
}

func NewMemoizerOnDelete(valueExpiration, errExpiration time.Duration, f func(interface{}, error)) *Memoizer {
	return &Memoizer{
		Storage: ccache.New(ccache.Configure().OnDelete(func(item *ccache.Item) {
			entry := item.Value().(*entry)
			f(entry.value, entry.err)
		})),
		group:           singleflight.Group{},
		valueExpiration: valueExpiration,
		errExpiration:   errExpiration,
	}
}

type entry struct {
	value interface{}
	err   error
}

func (m *Memoizer) Clear() {
	m.Storage.Clear()
}

func (m *Memoizer) Delete(key string) {
	m.Storage.Delete(key)
}

func (m *Memoizer) Get(key string) (interface{}, error) {
	item := m.Storage.Get(key)
	if item == nil || item.Expired() {
		return nil, nil
	}
	entry := item.Value().(*entry)
	return entry.value, entry.err
}

// Memoize executes and returns the results of the given function, unless there was a cached value of the same key.
// Only one execution is in-flight for a given key at a time.
// The boolean return value indicates whether v was previously stored.
func (m *Memoizer) Memoize(key string, fn func() (interface{}, error)) (interface{}, error) {
	// Check cache
	item := m.Storage.Get(key)
	if item != nil && !item.Expired() {
		entry := item.Value().(*entry)
		return entry.value, entry.err
	}

	// Combine memoized function with a cache store
	value, err, _ := m.group.Do(key, func() (interface{}, error) {
		item := m.Storage.Get(key)
		if item != nil && !item.Expired() {
			entry := item.Value().(*entry)
			return entry.value, entry.err
		}

		value, err := fn()

		var expiration = m.valueExpiration
		if err != nil {
			expiration = m.errExpiration
		}

		if expiration > 0 {
			m.Storage.Set(key, &entry{value: value, err: err}, expiration)
		}
		return value, err
	})
	return value, err
}

// MemoizeWithTTL does the same as Memoize, but allows to specify entry expiration in memo function
func (m *Memoizer) MemoizeWithTTL(key string, fn func() (interface{}, time.Duration, error)) (interface{}, error) {
	// Check cache
	item := m.Storage.Get(key)
	if item != nil && !item.Expired() {
		entry := item.Value().(*entry)
		return entry.value, entry.err
	}

	// Combine memoized function with a cache store
	value, err, _ := m.group.Do(key, func() (interface{}, error) {
		item := m.Storage.Get(key)
		if item != nil && !item.Expired() {
			entry := item.Value().(*entry)
			return entry.value, entry.err
		}

		value, duration, err := fn()

		if duration > 0 {
			m.Storage.Set(key, &entry{value: value, err: err}, duration)
		}
		return value, err
	})
	return value, err
}
