package nativebl

import (
	"context"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/nativebl/model"
)

type StaticResolver struct {
	nodeAddr *model.NodeAddr
}

func NewStaticResolver(blAddr string) *StaticResolver {
	return &StaticResolver{
		nodeAddr: &model.NodeAddr{BLAddr: blAddr},
	}
}

func (r *StaticResolver) ResolveNodeAddr(ctx context.Context, nodeID string) (addr *model.NodeAddr, err error) {
	return r.nodeAddr, nil
}
