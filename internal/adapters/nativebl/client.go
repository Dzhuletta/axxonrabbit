package nativebl

import (
	"context"
	"io"
	"strings"
	"time"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/domain/entities"

	"bitbucket.org/Axxonsoft/bl/events"
	"bitbucket.org/Axxonsoft/bl/primitive"
	"github.com/golang/protobuf/ptypes"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"

	"bitbucket.org/Axxonsoft/bl/domain"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/util"

	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/nativebl/an"
	"bitbucket.org/Axxonsoft/bl/nativebl"
)

type Client struct {
	client         *an.NativeBlClient
	events         entities.EventChannel
	nodeName       string
	subscriptionID string
}

func NewClient(address, user, password string, events entities.EventChannel) *Client {
	staticResolver := NewStaticResolver(address)
	blAuthorizer := nativebl.NewLoginPassAuthorizer(user, password)
	client := an.NewNativeBlClient(staticResolver, blAuthorizer)
	nodeName, err := getCurrentNodeName(client)
	util.CheckErr(err, "failed getting node name")
	return &Client{
		client:         client,
		events:         events,
		nodeName:       nodeName,
		subscriptionID: "",
	}
}

func getCurrentNodeName(client *an.NativeBlClient) (nodeName string, err error) {
	logger.Debug("Getting current node name started")
	err = client.DoWithNgpNodeService(context.Background(), "", func(ctx context.Context, domainService domain.NgpNodeServiceClient) error {
		selfInfoResponse, err := domainService.GetSelfInfo(ctx, &domain.GetSelfInfoRequest{})
		if err != nil {
			return err
		}
		nodeName = selfInfoResponse.Status.NodeName
		return nil
	})
	if err != nil {
		return "", err
	}

	logger.Debug("Got node name", "nodeName", nodeName)
	return nodeName, nil
}

func (c *Client) ReadEvents(beginTime, endTime time.Time, limit uint64) ([]*events.DetectorEvent, error) {
	searchFilters := append(make([]*events.SearchFilter, 0), &events.SearchFilter{Type: events.EEventType_ET_DetectorEvent})
	arraySearchFilters := &events.SearchFilterArray{Filters: searchFilters}
	var reader events.EventHistoryService_ReadEventsClient
	err := c.client.DoWithEventHistoryServiceWithoutTimeout(context.Background(), c.nodeName, func(ctx context.Context, eventHistoryReader events.EventHistoryServiceClient) error {
		timeRange := &primitive.TimeRange{BeginTime: an.Time2ANUnix(beginTime), EndTime: an.Time2ANUnix(endTime)}
		eventRequest := &events.ReadEventsRequest{Filters: arraySearchFilters, Range: timeRange, Limit: limit, Descending: false}

		var errService error
		reader, errService = eventHistoryReader.ReadEvents(ctx, eventRequest)
		if errService != nil {
			return errService
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return c.readNBLEvents(reader)
}

func (c *Client) readNBLEvents(reader events.EventHistoryService_ReadEventsClient) ([]*events.DetectorEvent, error) {
	logger.Debug("Start reading events")

	blEvents, err := reader.Recv()
	if err == io.EOF {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	detectorEvents := make([]*events.DetectorEvent, 0, len(blEvents.Items))
	for _, eventRaw := range blEvents.Items {
		if eventRaw.EventType != events.EEventType_ET_DetectorEvent {
			continue
		}

		event := &events.DetectorEvent{}
		if err := ptypes.UnmarshalAny(eventRaw.Body, event); err != nil {
			logger.Error("failed parsing event", "error", err)
			continue
		}

		detectorEvents = append(detectorEvents, event)
	}

	return detectorEvents, nil
}

func (c *Client) SendEvents(events []*events.DetectorEvent) {
	var (
		counter    uint64
		startTime  = an.Time2ANUnix(time.Now().UTC())
		latestTime string
	)

	for _, event := range events {
		// Send event to channel
		c.events <- entities.Event(*event)

		if strings.Compare(event.Timestamp, startTime) < 0 {
			startTime = event.Timestamp
		}

		if strings.Compare(event.Timestamp, latestTime) > 0 {
			latestTime = event.Timestamp
		}

		counter++
	}

	if counter > 0 {
		logger.Info("Processed events", "number", counter, "startTime", startTime, "latestTime", latestTime)
	}
}

func (c *Client) Close() {
	node, err := c.client.NodeAddressResolver.ResolveNodeAddr(context.Background(), c.nodeName)
	if err != nil {
		logger.Error("failed resolving node address")
		return
	}
	c.client.ClearConnCache(context.Background(), node.GetBlAddress())
	logger.Info("NativeBL client closed")
}
