package an

import (
	"time"
)

type Options interface {
	Apply(*NativeBlClient)
}

type maxRetryTimesOption struct {
	maxTimes int
}

type timeout struct {
	timeout time.Duration
}

func (t timeout) Apply(client *NativeBlClient) {
	client.grpcTimeout = t.timeout
}

func (m *maxRetryTimesOption) Apply(client *NativeBlClient) {
	client.maxTimes = m.maxTimes
}

func WithMaxRetryTimes(n int) Options {
	return &maxRetryTimesOption{maxTimes: n}
}

func WithTimeout(t time.Duration) Options {
	return &timeout{timeout: t}
}
