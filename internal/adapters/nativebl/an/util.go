package an

import (
	"time"

	"bitbucket.org/Axxonsoft/bl/config"
	"github.com/go-errors/errors"
)

const (
	UnitTypeDomain            = "Domain"
	UnitTypeNode              = "Node"
	UnitTypeDeviceIpint       = "DeviceIpint"
	UnitTypeVideoChannel      = "VideoChannel"
	UnitTypeStreaming         = "Streaming"
	UnitTypeMultimediaStorage = "MultimediaStorage"
	UnitTypeArchiveVolume     = "ArchiveVolume"
	UnitTypeArchiveContext    = "ArchiveContext"
	UnitTypeAvDetector        = "AVDetector"
	UnitTypeVisualElement     = "VisualElement"
	UnitTypeVMDASource        = "Source"
	UnitTypeAxxonDataLoader   = "AxxonDataLoader"
	UnitTypeEmbeddedStorage   = "EmbeddedStorage"
	UnitTypeAppDataDetector   = "AppDataDetector"
	UnitTypeVMDA              = "VMDA_DB"
)

const (
	DetectorTypeQueueDetector        = "QueueDetector"
	DetectorTypePeopleCounter        = "PeopleCounter"
	DetectorTypeLPRVIT               = "LprDetector_Vit"
	DetectorTypeFaceDetector         = "TvaFaceDetector"
	DetectorTypeHeatmap              = "SceneDescription"
	DetectorTypeMotion               = "MotionDetection"
	DetectorTypeTampering            = "SignalAudioDetection"
	DetectorTypePositionChange       = "SceneChange"
	DetectorTypeQualityLoss          = "QualityDegradation"
	DetectorTypeImageNoise           = "QualityDegradation_v2"
	DetectorTypeBlurredImage         = "BlurredDegradation"
	DetectorTypeCompressionArtifacts = "CompressedDegradation"
	DetectorTypeLineCrossing         = "CrossOneLine"
	DetectorTypeMotionInArea         = "MoveInZone"
	DetectorTypeStopInArea           = "StopInZone"
	DetectorTypeAppearanceInZone     = "ComeInZone"
	DetectorTypeDisappearanceInZone  = "OutOfZone"
	DetectorTypeLoitering            = "LongInZone"
	DetectorTypeUnattendedObject     = "LostObject"
	DetectorTypeMultipleObject       = "LotsObjects"
	DetectorTypeObjectTracker        = "SceneDescription"
	DetectorTypeFaceEvasion          = "FaceEvasionDetector"
)

const timeANFormat = "20060102T150405.000000"

type DescriptorPredicate func(unit *config.UnitDescriptor) bool
type UnitPredicate func(unit *config.Unit) bool

func getAddedID(resp *config.ChangeConfigResponse, err error) (string, error) {
	if err != nil {
		return "", err
	}
	if len(resp.Failed) != 0 {
		return "", errors.New("an.create.unit.failed")
	}
	if len(resp.Added) != 1 {
		return "", errors.Errorf("an.create.unit.empty.added.%v", resp.Added)
	}

	return resp.Added[0], nil
}

func Time2ANUnix(t time.Time) string {
	return t.Format(timeANFormat)
}
