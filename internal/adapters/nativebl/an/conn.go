package an

import (
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/logger"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/nativebl/memoize"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/nativebl/model"
	"bitbucket.org/Axxonsoft/AxxonRabbit/internal/adapters/util"
	"bitbucket.org/Axxonsoft/bl/events"

	"bitbucket.org/Axxonsoft/bl/config"
	"bitbucket.org/Axxonsoft/bl/domain"
	"bitbucket.org/Axxonsoft/bl/nativebl"
	"bitbucket.org/Axxonsoft/bl/realtimerecognizer"
	"bitbucket.org/Axxonsoft/bl/security"

	"context"
	"crypto/x509"
	"io"
	"time"

	"github.com/go-errors/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const AnonNBLKey = `
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAzLhprNSyvjcuiw166J15fBRkQU1AAHS2PHp80I90z62GMPL+
cS3K5QxudC2OaWnxZb2gbcpBNFzh7+tJUEzq/wI7lRKaKtiGtB1FaAKumPghkhd2
/Tr8VTCrCY6FgJlTKqkcAv78VKeBBNonGpccN8nUbGvXJMXSzAk7bImwDRQXNBCv
uRG0FQtmE2oAoEWLXG0gH8EW1QztrFy9fHN/eR+Rcwzic4T4dvBYHZZNBX3VjPOi
u6OIK11DqrGJi3e9TN1QUcN4C9dpqldxPgvx9wgFncog9jD7HwJQ/DE/IMAoVHwZ
410KsN+o4tLJjkTVsEcW7rkbbJaBHfhD+yqmqQIDAQABAoIBAGwY0Avnf746ywO3
iXe8dwJSjiGBFdNnzTYgAznpFef3G01LcZj3chQBvWzxBSqaO1HakBSI4GcyyEZz
+ZFCHC+s0SNE4EzRakc+0YA7MlApbSiD5VuPQuAEw7tXGx0tWKHFC7p1Q227yL90
JYO/2LGIi/b39nE/V7BPC6ajCWfNizFNcpgcPWgwonf73j0mG/kbD8R9Cazi/7J9
MZ60t2US/Ge3vJMlN3SBrXWNIUYVtCrVmeArEbcWohcEXKpQu3QlCro89r2pOyh+
rXCiVu4hRmDj/Z6CzOymMDbvQP9imNuBwMATeJaP8eTsW2R1tGte4msISKtW9Ua1
JPKiGRECgYEA5vZO5e9tner8ugJ9etPgYcOuvfCpCyDFHBuR5tnvPUY3JyNL2AHW
99LUDB96TZdowK2Vdbf4wtGoNDK5tPtrI5LheuAU1UDs75kYFjWBCC90QHcZsjYj
iZhA/4Zv8g618MB7DgWjzedgX4m8hK6fCwYMBqyC7sBhC0HZmZY3XfUCgYEA4unW
25y3K0RXX2IzZipYJh48fKgSOPMXDjlHCA1+TXmAhHM3y4yOrut9nWVipddjzKTh
/dVA+8wAFHb3TrPMutUbkjfqTJMIiQrhFC69LUnIsKaSYQu6iIuA8Zspg5ZcCOHz
LldMBs4nY+KBlhKtP9/vslCeBtQV6WaYetueIWUCgYEA4lhzHA6YS9I2WYkFNjGc
zdL7VnQbBqfX5GL0itv8BP3iIT4LHyc2aGs6mqLitlXzOBklx1dDuJHFmVo4+zAG
YLeauFQQtqnJSkqr+1/2E3KGKINQMIG0NC19Ta6P2RYnokjQj/5g+PKPVTHkCdgk
T6ZedM/uBVOOB31CZq17l10CgYEAkLok2B20llwYfjXcIqWPY4CVg8yPEtO5ONE/
hNtWW4PLfK8gPyt/NgHtNJ6dtLkUZkoj+goyUGdlBah7RC9ZvTB/TVtsjTqajw3p
UT4eWuxcnI8hfdRFPSH9NByK9erI+MFkoYH8c7q5VPP7QwTWi48BCvJwWFqdPyi4
yGObECECgYEAwQcfjhMPvDCMYyMvAyCTTH4hYc87Lk6r+xfwLkOJFSp5wNE94105
EuObIzr8rTZ8sjwHfMZkdaH2Pk/b34TxMBVXFuQBte5Nn5+vHaV8mrlClkf+8Us6
tCikE6sSudHeAi2nr2B4ngwcMieAqZyDeqt6sEzCFEAZSW2F8quEGXg=
-----END RSA PRIVATE KEY-----
`

const AnonNBLCrt = `
-----BEGIN CERTIFICATE-----
MIIDaTCCAlGgAwIBAgIJAJencyQhQ8qbMA0GCSqGSIb3DQEBCwUAMEoxCzAJBgNV
BAYTAkdCMQ8wDQYDVQQHDAZMb25kb24xFjAUBgNVBAoMDUF4eG9uU29mdCBMdGQx
EjAQBgNVBAMMCUFub255bW91czAgFw0xNzExMDQxNzQ2MjRaGA8yMTE3MTAxMTE3
NDYyNFowSjELMAkGA1UEBhMCR0IxDzANBgNVBAcMBkxvbmRvbjEWMBQGA1UECgwN
QXh4b25Tb2Z0IEx0ZDESMBAGA1UEAwwJQW5vbnltb3VzMIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEAzLhprNSyvjcuiw166J15fBRkQU1AAHS2PHp80I90
z62GMPL+cS3K5QxudC2OaWnxZb2gbcpBNFzh7+tJUEzq/wI7lRKaKtiGtB1FaAKu
mPghkhd2/Tr8VTCrCY6FgJlTKqkcAv78VKeBBNonGpccN8nUbGvXJMXSzAk7bImw
DRQXNBCvuRG0FQtmE2oAoEWLXG0gH8EW1QztrFy9fHN/eR+Rcwzic4T4dvBYHZZN
BX3VjPOiu6OIK11DqrGJi3e9TN1QUcN4C9dpqldxPgvx9wgFncog9jD7HwJQ/DE/
IMAoVHwZ410KsN+o4tLJjkTVsEcW7rkbbJaBHfhD+yqmqQIDAQABo1AwTjAdBgNV
HQ4EFgQUdRGQ83k1T57/uaFMswDyHGfjxHQwHwYDVR0jBBgwFoAUdRGQ83k1T57/
uaFMswDyHGfjxHQwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAvKNb
lR2hvMRRTZ/1hkORxKAHbglaxtOrTzMyEGLUyWUsliMt4vtXpeagivvQxbI1dzOm
y/1/hi6kGQ/aMSV8sw23H2mbDY7jgPTAvn6++ly2mStfpPd8gGhg2zdWryGVLXih
oNX40yASO2yDIDsbrsoNfu21vEhfK2WPG4xb1bk5Y+NCHXf19pBGm0qUrxg75IBH
C8SwFvPYezxCW68QuLCtPuG8nm7R22j1Gexz4cHrGvj45MRe/Ez73Zfu/uReIyj2
bnSNNu86vEk9gIXfAjuWOWivfBmfYV6pa718cWTClW2z2xlfTrW9Ft8hCqfJ3zUZ
l2hHTkJd4NqIUHblMQ==
-----END CERTIFICATE-----
`

const blConnExpire = time.Hour

const waitUntilInterval = time.Second * 2

// NodeAddressResolver resolves nodeId to physical connect address via supervisor API
// Don't mix up with NodeAddressGetter that retrieves serverName that we currently connected to
type NodeAddressResolver interface {
	ResolveNodeAddr(ctx context.Context, nodeID string) (addr *model.NodeAddr, err error)
}

type NativeBlClient struct {
	ConnCache           *memoize.Memoizer
	NodeAddressResolver NodeAddressResolver
	authorizer          nativebl.Authorizer

	// opts below
	maxTimes          int
	additionalHeaders []string
	serverName        string
	cert              string
	grpcTimeout       time.Duration
}

func NewNativeBlClient(nodeAddressResolver NodeAddressResolver, authorizer nativebl.Authorizer, opts ...Options) *NativeBlClient {
	maxTimes := 10

	nblClient := &NativeBlClient{
		ConnCache:           newMemoizer(),
		NodeAddressResolver: nodeAddressResolver,
		authorizer:          authorizer,
		maxTimes:            maxTimes,
		serverName:          "Anonymous",
		cert:                AnonNBLCrt,
		grpcTimeout:         defaultGRPCDuration,
	}
	for _, opt := range opts {
		opt.Apply(nblClient)
	}
	return nblClient
}

func newMemoizer() *memoize.Memoizer {
	return memoize.NewMemoizerOnDelete(blConnExpire, 0, func(value interface{}, err error) {
		if err != nil {
			return
		}
		conn := value.(*grpc.ClientConn)
		_ = conn.Close()
	})
}

func (c *NativeBlClient) ChangeConfig(ctx context.Context, nodeName string, req *config.ChangeConfigRequest) (string, error) {
	return getAddedID(c.SendChangeConfigRequest(ctx, nodeName, req))
}

func (c *NativeBlClient) SendChangeConfigRequest(ctx context.Context, nodeName string, req *config.ChangeConfigRequest) (resp *config.ChangeConfigResponse, err error) {
	err = c.DoWithConfigService(ctx, nodeName, func(ctx context.Context, configService config.ConfigurationServiceClient) error {
		resp, err = configService.ChangeConfig(ctx, req)
		if err != nil {
			return errors.New(err)
		}
		return nil
	})
	logger.Debug("sendChangeConfigRequest", "req", req, "resp", resp)
	return resp, err
}

func (c *NativeBlClient) SendChangeTemplatesRequest(ctx context.Context, nodeName string, req *config.ChangeTemplatesRequest) (resp *config.ChangeTemplatesResponse, err error) {
	err = c.DoWithConfigService(ctx, nodeName, func(ctx context.Context, configService config.ConfigurationServiceClient) error {
		resp, err = configService.ChangeTemplates(ctx, req)
		if err != nil {
			return errors.New(err)
		}
		return nil
	})
	logger.Debug("sendChangeTemplatesRequest", "req", req, "resp", resp)
	return resp, err
}

func (c *NativeBlClient) DoWithConfigService(ctx context.Context, nodeName string, f func(ctx context.Context, configService config.ConfigurationServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnection(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, config.NewConfigurationServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithSecurityService(ctx context.Context, nodeName string, f func(ctx context.Context, configService security.SecurityServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnection(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, security.NewSecurityServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithRealtimeRecognizerService(ctx context.Context, nodeName string, f func(ctx context.Context, recognizer realtimerecognizer.RealtimeRecognizerServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnectionWithoutTimeout(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, realtimerecognizer.NewRealtimeRecognizerServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithDomainNotifierServiceWithoutTimeout(ctx context.Context, nodeName string, f func(ctx context.Context, domainNotifier events.DomainNotifierClient) error, maxRepeat ...int) error {
	return c.DoWithConnectionWithoutTimeout(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, events.NewDomainNotifierClient(conn))
	}, maxRepeat...)
}
func (c *NativeBlClient) DoWithEventHistoryServiceWithoutTimeout(ctx context.Context, nodeName string, f func(ctx context.Context, eventHistoryReader events.EventHistoryServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnectionWithoutTimeout(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, events.NewEventHistoryServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithDomainService(ctx context.Context, nodeName string, f func(ctx context.Context, domainService domain.DomainServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnection(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, domain.NewDomainServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithNgpNodeService(ctx context.Context, nodeName string, f func(ctx context.Context, domainService domain.NgpNodeServiceClient) error, maxRepeat ...int) error {
	return c.DoWithConnectionWithoutTimeout(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, domain.NewNgpNodeServiceClient(conn))
	}, maxRepeat...)
}

func (c *NativeBlClient) DoWithDomainNotifierService(ctx context.Context, nodeName string, f func(ctx context.Context, domainNotifier events.DomainNotifierClient) error, maxRepeat ...int) error {
	return c.DoWithConnection(ctx, nodeName, func(ctx context.Context, conn *grpc.ClientConn) error {
		return f(ctx, events.NewDomainNotifierClient(conn))
	}, maxRepeat...)
}

const defaultGRPCDuration = time.Minute * 20 //todo dananita

func (c *NativeBlClient) DoWithConnection(ctx context.Context, nodeName string, f func(ctx context.Context, conn *grpc.ClientConn) error, maxRepeat ...int) (err error) {
	return c.doWithConnectionCommon(ctx, nodeName, f, c.grpcTimeout, maxRepeat...)
}

func (c *NativeBlClient) DoWithConnectionWithoutTimeout(ctx context.Context, nodeName string, f func(ctx context.Context, conn *grpc.ClientConn) error, maxRepeat ...int) (err error) {
	return c.doWithConnectionCommon(ctx, nodeName, f, 0, maxRepeat...)
}

func (c *NativeBlClient) ClearConnCache(ctx context.Context, blAddr string) {
	c.removeConnection(blAddr)
}

type grpcFunc func(ctx context.Context, conn *grpc.ClientConn) error

func (c *NativeBlClient) doWithConnectionCommon(ctx context.Context, nodeName string, f grpcFunc, grpcDuration time.Duration, maxRepeat ...int) (err error) {
	maxTimes := c.maxTimes
	if len(maxRepeat) > 0 {
		maxTimes = maxRepeat[0]
	}

	return util.WaitUntil(ctx, maxTimes, waitUntilInterval, 0, "doWithConnection", func(ctx context.Context) (bool, error) {
		node, err := c.NodeAddressResolver.ResolveNodeAddr(ctx, nodeName)
		if err != nil {
			return false, err
		}

		conn, err := c.getConnection(ctx, node.GetBlAddress())
		if err != nil {
			return false, err
		}

		if len(c.additionalHeaders) > 0 {
			ctx = metadata.AppendToOutgoingContext(ctx, c.additionalHeaders...)
		}

		if grpcDuration == 0 {
			err = f(ctx, conn)
		} else {
			func() {
				ctx, cancelFunc := context.WithTimeout(ctx, grpcDuration)
				defer cancelFunc()
				err = f(ctx, conn)
			}()
		}

		if err != nil {
			errCode := getGRPCStatus(err) // f can wrap error with go-errors *Error
			logger.Error("doWithConnection func error",
				"error", err,
				"code", errCode, "node", node,
			)

			switch errCode {
			case codes.DeadlineExceeded, codes.Canceled, codes.Unimplemented:
				// context canceled so just exit from method
				return true, err
			case codes.Unavailable, codes.Unauthenticated:
				// connection error, need to reconnect
				c.removeConnection(node.GetBlAddress())
			}

			return false, err
		}
		return true, nil
	})
}

func (c *NativeBlClient) getConnection(ctx context.Context, blAddr string) (conn *grpc.ClientConn, err error) {
	connVal, err := c.ConnCache.Memoize(blAddr, func() (nodeAddr interface{}, err error) {
		conn, err = c.MakeConnection(ctx, blAddr, c.grpcTimeout)
		if err != nil {
			logger.Error("connection to bl", "error", err, "address", blAddr)
		} else {
			logger.Info("NativeBL connected successfully", "address", blAddr)
		}
		return conn, err
	})

	if err != nil {
		return nil, err
	}
	return connVal.(*grpc.ClientConn), nil
}

func (c *NativeBlClient) removeConnection(blAddr string) {
	c.ConnCache.Delete(blAddr)
}

func (c *NativeBlClient) WaitForNBLStarts() error {
	return c.DoWithDomainService(context.Background(), "", func(ctx context.Context, domainService domain.DomainServiceClient) error {
		_, err := domainService.GetHostPlatformInfo(ctx, &domain.GetHostPlatformInfoRequest{})
		return err
	}, 50)
}

// MakeConnection makes grpc connection to server
func (c *NativeBlClient) MakeConnection(ctx context.Context, serviceAddress string, connTimeout time.Duration) (conn *grpc.ClientConn, err error) {
	if len(c.additionalHeaders) > 0 {
		ctx = metadata.AppendToOutgoingContext(ctx, c.additionalHeaders...)
	}
	timeoutCtx, cancel := context.WithTimeout(ctx, connTimeout)
	defer cancel()

	keepaliveOpt := grpc.WithKeepaliveParams(keepalive.ClientParameters{
		Time:                20 * time.Second,
		PermitWithoutStream: true,
	})
	opts := []grpc.DialOption{c.certOpt(), keepaliveOpt}
	opts = append(opts, c.logOpt()...)

	if c.authorizer != nil {
		creds, err := c.authorizer.GetTokenCredential(timeoutCtx, serviceAddress)
		if err != nil {
			return nil, err
		}
		opts = append(opts, grpc.WithPerRPCCredentials(creds))
	}
	return grpc.DialContext(timeoutCtx, serviceAddress, opts...)
}

func (c *NativeBlClient) logOpt() []grpc.DialOption {
	ui := grpc.WithUnaryInterceptor(func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		err := invoker(ctx, method, req, reply, cc, opts...)
		if err != nil {
			logger.Error("failed unary nbl request", "error", err, "req", req, "reply", reply, "method", method)
		} else {
			logger.Debug("Unary nbl request", "req", req, "reply", reply, "method", method)
		}
		return err
	})
	si := grpc.WithStreamInterceptor(func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		logger.Debug("Stream nbl request", "method", method)

		s, err := streamer(ctx, desc, cc, method, opts...)
		if err != nil {
			return nil, err
		}
		return newWrappedStream(s), err
	})

	return []grpc.DialOption{ui, si}
}

type wrappedStream struct {
	grpc.ClientStream
}

func (w *wrappedStream) RecvMsg(m interface{}) error {
	err := w.ClientStream.RecvMsg(m)
	if err == io.EOF {
		logger.Debug("Received message via nbl", "message", "EOF")
	} else {
		if err != nil {
			logger.Error("failed receiving message via nbl", "error", err)
		} else {
			logger.Debug("Received message via nbl")
		}
	}
	return err
}

func (w *wrappedStream) SendMsg(m interface{}) error {
	err := w.ClientStream.SendMsg(m)
	if err == io.EOF {
		logger.Debug("Sent message via nbl", "message", "EOF")
	} else {
		if err != nil {
			logger.Error("failed sending message via nbl", "error", err, "message", m)
		} else {
			logger.Debug("Sent message via nbl", "message", m)
		}
	}
	return err
}

func newWrappedStream(s grpc.ClientStream) grpc.ClientStream {
	return &wrappedStream{ClientStream: s}
}

func (c *NativeBlClient) certOpt() grpc.DialOption {
	if c.cert == "" || c.serverName == "" {
		return grpc.WithInsecure()
	}
	cp := x509.NewCertPool()
	util.CheckOk(cp.AppendCertsFromPEM([]byte(c.cert)), "credentials: failed to append certificates")
	transportCredentials := credentials.NewClientTLSFromCert(cp, c.serverName)

	return grpc.WithTransportCredentials(transportCredentials)
}

func getGRPCStatus(err error) codes.Code {
	if status.Code(err) != codes.Unknown {
		return status.Code(err)
	}
	if wrapper, ok := err.(*errors.Error); ok {
		return status.Code(wrapper.Err)
	}
	return codes.Unknown
}
