#!/bin/bash

DirName=axxonrabbit_$(date +%Y%m%d)

rm -fR $DirName
mkdir $DirName

mkdir $DirName/bin
cp bin/eventexporterservice.exe $DirName/bin

mkdir $DirName/configs
cp configs/eventexporterservice.yml $DirName/configs

mkdir $DirName/logs

cp scripts/StartService.bat $DirName
cp scripts/StopService.bat $DirName

# Make archive
mkdir deployment
tar -zcvf deployment/$DirName.tar.gz $DirName

# Delete temporary files
rm -fR $DirName