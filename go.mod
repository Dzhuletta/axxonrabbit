module bitbucket.org/Axxonsoft/AxxonRabbit

go 1.13

require (
	bitbucket.org/Axxonsoft/bl v1.4.9827
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/go-errors/errors v1.1.1
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/karlseguin/ccache v2.0.3+incompatible
	github.com/karlseguin/expect v1.0.7 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c
	google.golang.org/grpc v1.25.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
