dep:
	GOSUMDB=off GOPROXY=http://npm.itvgroup.ru:3000 go mod download

run-rabbitmq: stop-rabbitmq
	docker run -d --hostname my-rabbit -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 15672:15672 --name some-rabbit rabbitmq:3.7.5
	docker exec some-rabbit rabbitmq-plugins enable rabbitmq_management

stop-rabbitmq:
	docker rm -f some-rabbit || true

build-eventexporter:
	GO111MODULE=on GOPRIVATE=bitbucket.org/Axxonsoft/bl CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
	go build -gcflags="all=-N -l" -o ./bin/eventexporter -v ./cmd/eventexporter

build-eventexporter-win:
	GO111MODULE=on GOPRIVATE=bitbucket.org/Axxonsoft/bl CGO_ENABLED=0 GOOS=windows GOARCH=amd64 \
	go build -gcflags="all=-N -l" -o ./bin/eventexporter.exe -v ./cmd/eventexporter

build-eventexporterservice-win: dep
	GO111MODULE=on GOPRIVATE=bitbucket.org/Axxonsoft/bl CGO_ENABLED=0 GOOS=windows GOARCH=amd64 \
	go build -gcflags="all=-N -l" -o ./bin/eventexporterservice.exe -v ./cmd/eventexporterservice

deployment: build-eventexporterservice-win
	scripts/deployment.sh

